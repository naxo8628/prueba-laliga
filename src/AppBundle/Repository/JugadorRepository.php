<?php

namespace AppBundle\Repository;

/**
 * JugadorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class JugadorRepository extends \Doctrine\ORM\EntityRepository
{
    public function getPlayersNoClub($clubId = null){

            $qb = $this
            ->createQueryBuilder('e')
            ->select('e.nombre')->addSelect('e.id ')
            ->where('e.club is null');

            if($clubId != null){
                $qb->where('e.club = :clubId')
                    ->setParameter('clubId',$clubId);
            }
        return $qb->getQuery()
            ->getResult();
    }
}
